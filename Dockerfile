# Si on modifie le tag ici, il faut aussi le modifier dans gitlab-ci.yaml pour rester cohérent
FROM mongo:3.4
USER 1500:1500
CMD ["mongod"]
